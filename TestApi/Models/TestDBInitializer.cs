﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace TestApi.Models
{
    public class TestDBInitializer : CreateDatabaseIfNotExists<TestContext>
    {
        protected override void Seed(TestContext context)
        {
            IList<User> users = new List<User>();

            users.Add(new User() { Name = "Administrador", Phone = "+5543998432882", Email = "josematheusnoveli@gmail.com", Password = "5151**", DT_Birth = DateTime.Now.AddYears(-19) });
            users.Add(new User() { Name = "Bolinha", Phone = "+5543998432882", Email = "josematheusnoveli@gmail.com", Password = "5151**", DT_Birth = DateTime.Now.AddYears(-19) });

            context.Users.AddRange(users);

            base.Seed(context);
        }
    }
}