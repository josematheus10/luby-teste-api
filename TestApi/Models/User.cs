﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TestApi.Filters;

namespace TestApi.Models
{
    // FIZ ISSO DE FORMA SIMPLES (ESTAVA SEM TEMPO)...
    // BOAS PRATICAS É MELHOR UTILIZAR DTO E VIEWMODEL
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Informe o seu nome")]
        public string Name { get; set; }

        [Phone]
        [Display(Name = "Phone")]
        public string Phone { get; set; }


        [Required(ErrorMessage = "Informe o seu email")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Informe um email válido")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [PasswordPropertyText]
        [Required(ErrorMessage = "Informe uma senha")]
        [CheckPassword]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        [MinAge(18)]
        [Display(Name = "DT_Birth")]
        public DateTime DT_Birth { get; set; }
    }
}