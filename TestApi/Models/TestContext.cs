﻿using System.Data.Entity;
using TestApi.Models;

namespace TestApi.Models
{
    //code first :)
    public class TestContext : DbContext
    {
        public TestContext() : base("TestDB")
        {
            Database.SetInitializer<TestContext>(new TestDBInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<User>().ToTable("TB_USER");
        }

        public DbSet<User> Users { get; set; }
    }
}