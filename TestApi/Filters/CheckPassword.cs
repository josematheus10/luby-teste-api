﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace TestApi.Filters
{
    public class CheckPassword : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            bool validPassword = false;
            string reason = String.Empty;
            string Password = value == null ? String.Empty : value.ToString();
            if (String.IsNullOrEmpty(Password) || Password.Length < 6)
            {
                reason = "Sua nova senha deve ter pelo menos 6 caracteres.";
            }
            else
            {
                Regex reSymbol = new Regex("[^a-zA-Z0-9]");
                if (!reSymbol.IsMatch(Password))
                {
                    reason += "Sua nova senha deve conter pelo menos 1 caractere de especial.";
                }
                else
                {
                    validPassword = true;
                }
            }
            if (validPassword)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(reason);
            }
        }
    }
}