﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestApi.Models;

namespace TestApi.Controllers
{
    public class UsersController : ApiController
    {
        public TestContext context = new TestContext();

        // GET api/users
        public IEnumerable<User> Get()
        {
             return context.Users;
        }

        // GET api/users/5
        public User Get(int id)
        {
            return context.Users.Find(id);
        }

        // POST api/users
        public bool Post([FromBody]User user)
        {
            if (user == null)
            return false;

            context.Users.Add(user);
            context.SaveChanges();
            return true;
        }

        // PUT api/users/5
        public bool Put(int id, [FromBody]User user)
        {
            User userUpdate = context.Users.FirstOrDefault(x => x.Id == id);

            if (userUpdate == null)
            {
                return false;
            }

            //AutoMapper fez falta aqui sorry
            userUpdate.Name = user.Name;
            userUpdate.Phone = user.Phone;
            userUpdate.Email = user.Email;
            userUpdate.Password = user.Password;
            userUpdate.DT_Birth = user.DT_Birth;

            context.SaveChanges();
            return true;
        }

        // DELETE api/users/5
        public bool Delete(int id)
        {
            var user = context.Users.Find(id);

            if (user != null)
            {
                context.Users.Remove(user);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }
    }
}
